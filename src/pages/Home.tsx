import { useRef, useState } from "react";
import { TodoItemType } from "../App";
import TodoItem from "../TodoItem";
import "./Home.css";

export default function Home() {
  const [task, setTask] = useState("");
  const [todos, setTodos] = useState<TodoItemType[]>([]);
  const taskRef = useRef<HTMLInputElement>(null);

  const onTodoChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTask(e.target.value);
  };

  const checkedTodosCount = todos.filter((t) => t.checked).length;

  return (
    <>
    <div className="toDo-container">
      <h2>To-Do List</h2>
      <input type="text" ref={taskRef} value={task} onChange={onTodoChange} className="toDo-input" />
      <button 
        className="add-toDo"
        onClick={() => {
          setTodos((todos) => [
            ...todos,
            { id: new Date().getTime(), task, checked: false },
          ]);
          setTask("");
          taskRef.current?.focus();
        }}
      >
        Add Todo
      </button>
      <button
        className="clear-toDo"
        onClick={() => {
          setTodos([]);
        }}
      >
        Clear Todos
      </button>
      <br />
      <p className="toDo-count">Checked Todos Count: {checkedTodosCount}</p>
      <br />
      <ul className="toDo-list">
        {todos.map((todo) => (
          <TodoItem
            key={todo.id}
            todo={todo}
            onCheckedChanged={(id, checked) => {
              setTodos((todos) =>
                todos.map((t) => (t.id === id ? { ...t, checked } : t))
              );
            }}
            onDelete={(id) => {
              setTodos((todos) => todos.filter((t) => t.id !== id));
            }}
          />
        ))}
      </ul>
      </div>
    </>
  );
}
