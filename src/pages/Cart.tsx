import { useCartContext } from "../contexts/CartContext";
import "./Cart.css";

export default function Cart() {
  const { cart, dispatch } = useCartContext();

  
  return (
    <>
      <div
        style={{ marginTop: "8px", display: "flex", flexDirection: "column" }}
      >
        {cart.map((cartItem) => (
          <div style={{ margin: "8px 0", display: "flex" }}>
            <img src={cartItem.image} 
            style={{height: "5rem"}}></img>
            <p>{cartItem.title}</p> <div style={{ flexGrow: 1 }} />
            <button 
              className="inc-btn"
              onClick={() => {
                dispatch({ type: "increaseQuantity", productId: cartItem.id });
              }}
            >
              +
            </button>
            <p>x{cartItem.quantity}</p>
            <button className="dec-btn"
            onClick={() => {
              dispatch({ type: "decreaseQuantity", productId: cartItem.id });
            }}>-</button>

            <button className="delete-btn"
            onClick={() => {
              dispatch({ type: "removeFromCart", productId: cartItem.id });
            }}>Remove</button>
          </div>
        ))}

        <div style={{ height: "32px" }} />
        <button
          style={{ color: "red" }}
          onClick={() => dispatch({ type: "clearCart" })}
        >
          Clear Cart
        </button>
      </div>
    </>
  );
}
