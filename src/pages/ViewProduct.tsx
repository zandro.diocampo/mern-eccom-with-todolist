import { FunctionComponent, useEffect, useState } from "react";
import { RouteComponentProps } from "wouter";
import { Product } from "./Products";
import "./ViewProduct.css";
import { useCartContext } from "../contexts/CartContext";


const ViewProduct: FunctionComponent<RouteComponentProps> = ({ params }) => {
  const [product, setProduct] = useState<Product | undefined>();
  
  const { dispatch } = useCartContext();

  useEffect(() => {
    fetch(`https://fakestoreapi.com/products/${params.productId}`)
      .then((res) => res.json())
      .then((json) => setProduct(json));
  }, [params.productId]);

  return (
    <>
      <h2>View Product</h2>
      {product && (
        <>
          <div className="product-div">
          <img src={product.image} style={{height: "500px"}}/>
          <div className="details-div">
            <p className="product-title">{product.title}</p>
            <p className="product-description">{product.description}</p>
            <p className="product-price">Price: ${product.price}</p> 
               <button
                style={{
                  width: "100%",
                  color: "white",
                  border: 'none'
                }} 
                className="add-to-cart-btn2"
                onClick={() => {
                  dispatch({ type: "addToCart", product });
                }}
              >
                Add to Cart
              </button>
          </div>
          </div>
        </>
      )}
    </>
  );
};

export default ViewProduct;
