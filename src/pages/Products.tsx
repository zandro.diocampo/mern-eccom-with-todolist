import { useEffect, useState } from "react";
import { useLocation } from "wouter";
import { useCartContext } from "../contexts/CartContext";
import './Products.css';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export type Product = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  rating: { rate: number; count: number };
  title: string;
};

export default function Products() {
  const [_, setLocation] = useLocation();
  const [products, setProducts] = useState<Product[]>([]);
  const { dispatch } = useCartContext();

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => setProducts(json));
  }, []);

  return (
    <>
      <div className="header-container">
        <h2>Products</h2>
      </div>

      <div className="productsContainer">
        {products.map((product) => (
          <div>
          <div className="itemContainer"
          >
            <img
              src={product.image}
              alt={product.title}
              style={{ width: "100%" }}
            />
            <p style={{ fontSize: ".8rem" }}>
              <strong>{product.title}</strong>
            </p>
            {/* <p style={{ fontSize: ".6rem" }}>{product.description}</p> */}
            </div>
            <div className="buttons">  
              <button
                style={{ width: "100%",
                         border: 'none', }}
                onClick={() => {
                  setLocation(`/products/${product.id}`);
                }}
              >
                View
              </button>
                <div style={{ margin: "8px" }} />
                <button
                style={{
                  width: "100%",
                  color: "white",
                  border: 'none'
                }} 
                className="add-to-cart-btn"
                onClick={() => {
                  dispatch({ type: "addToCart", product });
                  toast.success("Successfuly added to cart!", {
                    position: toast.POSITION.TOP_LEFT,
                    autoClose: false
                  });
                }}
              >
                Add to Cart
              </button>
            </div>
          </div>
        ))}
      </div>
      <ToastContainer />
    </>
  );
}
