import { Link } from "wouter";
import { useCartContext } from "./contexts/CartContext";
import cartImg from './images/cart-icon.png';
import './pages/CartCount.css';

export default function CartCount() {
  const { cart } = useCartContext();

  return (
    <>
    <div className="cart-container">
      <Link href="/cart">
        <img src={cartImg} className="cartImg"></img>
        <div className="cart-length">
          {cart.length}
        </div>
        </Link>
     </div>   
    </>
  );
}
